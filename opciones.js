//Librerias necesarias
const inquirer = require("inquirer");

//Opciones generales
const INICIO_S = 1;
const CREAR_C = 2;
const SALIR = 3;

//Opciones de inicio de sesión
const VER_G = 1;
const REGISTRAR_G = 2;
const BUSCAR_G = 3;
const VER_C = 4;
const CATEGORIAS=5;
const CREAR_CAT = 6;
const CERRAR_S = 7;

//opciones de buscar categoria
const EDITAR_CAT = 1;
const ELIMINAR_CAT = 2;
const VOLVER = 3;

//Opciones de buscar gasto
const EDITAR_GASTO = 1;
const ELIMINAR_GASTO = 2;
const SALIR_GASTO = 3;

/**
 * Datos de entrada para crear una nueva cuenta en la aplicación
 * @returns {object} Contiene el usuario, email y password de la nueva cuenta
 */
const crearNuevaCuenta = async () => {
  const opciones = [
    {
      name: "usuario",
      type: "input",
      message: "Digite el nombre de usuario",
    },
    {
      name: "email",
      type: "input",
      message: "Digite el correo electronico",
    },
    {
      name: "password",
      type: "input",
      message: "Digite una contraseña",
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 * Parámetros para entrar a una sesión
 * @returns {Object} Contiene el email y password de la sesión que se quiere iniciar
 */
const datosAcceso = async () => {
  const opciones = [
    {
      name: "email",
      type: "input",
      message: "Digite el email: ",
    },
    {
      name: "password",
      type: "password",
      message: "Digite el password: ",
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 * Dato para crear una nueva categoría
 * @returns {Object} Contiene el nombre de la nueva categoría que se quiere crear
 */
const nuevaCategoria = async () => {
  const opciones3 = [
    {
      name: "nombre",
      type: "input",
      message: "Nombre de la nueva categoría: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Datos para registrar un gasto
 * @returns {Object} Contiene los parámetros necesarios para poder crear un nuevo gasto
 * (gasto,descripción y fecha)
 */
const crearGasto = async () => {
  const opciones3 = [
    {
      name: "gasto",
      type: "input",
      message: "Valor del gasto ($): ",
    },
    {
      name: "descripcion",
      type: "input",
      message: "Motivo del gasto: ",
    },
    {
      name: "fecha",
      type: "date",
      message: "Fecha que del gasto (AAAA/MM/DD): ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Asociar un gasto con una categoria
 * @returns {Object} Respuesta del usuario que se presenta cuando el usuario crea un gasto
 */
const preguntaGasto = async () => {
  const opciones3 = [
    {
      name: "pregunta",
      type: "input",
      message: "Quiere asociar el gasto con una categoria (SI o NO): ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Categoria que esta asociada con un gasto
 * @returns {Object} Coniene el nombre de la categoría con la que estará asociada
 * el gasto
 */
const categoria_gasto = async () => {
  const opciones3 = [
    {
      name: "nombre",
      type: "input",
      message: "Nombre de la categoría: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Dato para editar una categoría ya registrada
 * @returns {Object} Contiene el nombre de la categoría que se va a cambiar y el nuevo
 * nombre
 */
const cambioNombreCategoria = async () => {
  const opciones4 = [
    {
      name: "categoria",
      type: "input",
      message: "Categoría a cambiar: ",
    },
    {
      name: "nombre",
      type: "input",
      message: "Nuevo nombre: ",
    },
  ];
  return inquirer.prompt(opciones4);
};

/**
 * Menu inicial de la aplicación
 * @returns {Object} Contiene primer menu que se le muestra al usuario
 */
const menu = async () => {
  const opciones = [
    {
      name: "opcion",
      type: "list",
      message: "Seleccione su opción",
      choices: [
        { value: VER_G, name: "INICIO DE SESIÓN" },
        { value: CREAR_C, name: "CREAR CUENTA" },
        { value: SALIR, name: "HASTA PRONTO" },
      ],
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 * Pregunta eliminar categoria
 * @returns {Object} Contiene el nombre de la categoría a eliminar
 */
const nombreparaEliminarCat = async () => {
  const opciones3 = [
    {
      name: "categoria",
      type: "input",
      message: "Escriba el nombre de la categoría a eliminar: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Pregunta de confirmación
 * @returns {Object} Respuesta del usuario para confirmar
 */
const preguntaConfirmacion = async () => {
  const opciones3 = [
    {
      name: "pregunta",
      type: "input",
      message: "¿Esta de acuerdo? (SI o NO): ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * menú de cada sesión
 * @returns Contiene el menú de cada sesión iniciada
 */
const menu_s = async () => {
  const opciones = [
    {
      name: "opcion2",
      type: "list",
      message: "Seleccione su opción",
      choices: [
        { value: VER_G, name: "VER GASTOS" },
        { value: REGISTRAR_G, name: "REGISTRAR GASTO" },
        { value: BUSCAR_G, name: "BUSCAR GASTO" },
        { value: VER_C, name: "VER CATEGORÍAS" },
        { value: CATEGORIAS, name: "CATEGORÍAS REGISTRADAS" },
        { value: CREAR_CAT, name: "CREAR CATEGORÍA" },
        { value: CERRAR_S, name: "CERRAR SESIÓN" },
      ],
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 *
 * @returns {object} Contiene el menú de buscar categoria
 */
const menu_cat = async () => {
  const opciones = [
    {
      name: "opcion2",
      type: "list",
      message: "Seleccione su opción",
      choices: [
        { value: EDITAR_CAT, name: "EDITAR CATEGORÍA" },
        { value: ELIMINAR_CAT, name: "ELIMINAR CATEGORÍA" },
        { value: VOLVER, name: "VOLVER" },
      ],
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 *
 * @returns {object} Contiene el menú para cada gasto
 */
const menuCadaGasto = async () => {
  const opciones = [
    {
      name: "opcion",
      type: "list",
      message: "Opciones para el gasto:",
      choices: [
        { value: EDITAR_GASTO, name: "EDITAR GASTO" },
        { value: ELIMINAR_GASTO, name: "ELIMINAR GASTO" },
        { value: SALIR_GASTO, name: "SALIR DEL GASTO" },
      ],
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 *
 * @param {Object} datos_gastos Contiene los datos registrados en el archivo datos_gastos_registro.JSON
 * @param {Object} inicioCuenta Contiene el email y password de la cuenta iniciada
 * @returns {Object} Contiene el menú de cada gasto registrado
 */
const menuDinamicoGastos = (datos_gastos, inicioCuenta) => {
  let gastos = datos_gastos.gastos_cuentas;
  const choices = [];
  for (const gasto of gastos) {
    if (gasto.email == inicioCuenta.email) {
      choices.push({
        name:
          "Categoría: " +
          gasto.categoria +
          "    " +
          "Gasto: " +
          gasto.gasto +
          "    " +
          "Descripción: " +
          gasto.descripcion +
          "    " +
          "Fecha: " +
          gasto.fecha,

        value:
          "Categoría: " +
          gasto.categoria +
          "    " +
          "Gasto: " +
          gasto.gasto +
          "    " +
          "Descripción: " +
          gasto.descripcion +
          "    " +
          "Fecha: " +
          gasto.fecha,
      });
    }
  }
  const opciones = [
    {
      name: "opcion",
      type: "list",
      message: "Seleccione el gasto",
      choices: choices,
    },
  ];
  return inquirer.prompt(opciones);
};

/**
 * Editar el gasto
 * @returns {Object}    Contiene el parámetro que quiere editar el usuario (una categoría,
 * gasto,descripción o fecha)
 */
const parametrodelGasto = async () => {
  const opciones3 = [
    {
      name: "pregunta",
      type: "input",
      message: "Digite el parámetro que quiere editar: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 *
 * @param {Object} parametro EL parámetro que se quiere editar
 * @returns {Object} Coniene el nuevo valor del parámetro a editar
 */
const nuevoValorParametroGasto = async (parametro) => {
  const opciones3 = [
    {
      name: "nombre",
      type: "input",
      message: parametro.pregunta + " es: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 *
 * @returns {Object} Contiene el párametro con que se va a filtrar los gastos
 * que se le muestran con la cuenta iniciada
 */
const parametroparafiltrar = async () => {
  const opciones3 = [
    {
      name: "filtro",
      type: "input",
      message: "Digite el parámetro del filtro: ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 *
 * @param {Object} vistaFiltro  El filtro que se utiliza para mostrar los gastos de la
 * cuenta iniciada ("categoria" o "mes")
 * @returns {Object} Coniene el nombre del filtro
 */
const nombredelfiltro = async (vistaFiltro) => {
  const opciones3 = [
    {
      name: "nombre",
      type: "input",
      message: "Nombre de " + vistaFiltro.filtro + ": ",
    },
  ];
  return inquirer.prompt(opciones3);
};

/**
 * Funciones y variables que son exportadas al archivo app.js
 */
module.exports = {
  menu,
  crearNuevaCuenta,
  datosAcceso,
  INICIO_S,
  CREAR_C,
  SALIR,
  menu_s,
  VER_G,
  REGISTRAR_G,
  BUSCAR_G,
  VER_C,
  CATEGORIAS,
  CREAR_CAT,
  CERRAR_S,
  nuevaCategoria,
  cambioNombreCategoria,
  menu_cat,
  EDITAR_CAT,
  ELIMINAR_CAT,
  VOLVER,
  crearGasto,
  preguntaGasto,
  categoria_gasto,
  nombreparaEliminarCat,
  preguntaConfirmacion,
  menuCadaGasto,
  menuDinamicoGastos,
  EDITAR_GASTO,
  ELIMINAR_GASTO,
  SALIR_GASTO,
  parametrodelGasto,
  nuevoValorParametroGasto,
  parametroparafiltrar,
  nombredelfiltro,
};
